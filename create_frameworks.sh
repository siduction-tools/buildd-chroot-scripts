#!/bin/sh

CHROOT_DIR=/srv/chroot/kdeframeworks
SCHROOT_NAME=kdeframeworks

#set chroot
eatmydata debootstrap --variant=buildd --include=eatmydata,ccache sid $CHROOT_DIR http://ftp.de.debian.org/debian

#set sources.list
echo "" >> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://packages.siduction.org/kdenext kde-frameworks-exp main" >> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://packages.siduction.org/kdenext kde-frameworks-exp main" >> $CHROOT_DIR/etc/apt/sources.list

#set siduction keyring
schroot --directory=/ -c $SCHROOT_NAME apt-get update
schroot --directory=/ -c $SCHROOT_NAME -- apt-get -y --force-yes install siduction-archive-keyring
schroot --directory=/ -c $SCHROOT_NAME apt-get update

#set ccache
schroot --directory=/ -c $SCHROOT_NAME dpkg-reconfigure ccache

#copy build-env.sh
cp /srv/chroot/build-env.sh $CHROOT_DIR

#copy apt preferences
cp preferences $CHROOT_DIR/etc/apt/preferences
