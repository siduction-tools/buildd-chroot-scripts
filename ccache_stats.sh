#!/bin/sh

ARCH=`dpkg-architecture -qDEB_HOST_ARCH`

schroot --directory=/ -c kdeframeworks -- /bin/sh -c "CCACHE_DIR=/var/cache/ccache-$ARCH/ ccache -s"
