#!/bin/sh

ARCH=`dpkg-architecture -qDEB_HOST_ARCH`

#clear cache
schroot --directory=/ -c kdeframeworks -- /bin/sh -c "CCACHE_DIR=/var/cache/ccache-$ARCH/ ccache -C"

#set size limit
schroot --directory=/ -c kdeframeworks -- /bin/sh -c "CCACHE_DIR=/var/cache/ccache-$ARCH/ ccache -M 10G"

#reset stats
schroot --directory=/ -c kdeframeworks -- /bin/sh -c "CCACHE_DIR=/var/cache/ccache-$ARCH/ ccache -z"
